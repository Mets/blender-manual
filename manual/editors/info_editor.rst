.. index:: Editors; Info Editor

***********
Info Editor
***********

The Info editor logs the executed operators as well as errors, warnings,
and informational messages. You can select an entry by clicking it,
optionally holding :kbd:`Shift` to add it to the existing selection.

.. figure:: /images/editors_info-editor_ui.png

   Info Editor.


Interface
=========

View Menu
---------

Area
   Area controls. See the :doc:`user interface </interface/window_system/areas>`
   documentation for more information.


Info Menu
---------

Select All :kbd:`A`
   Selects all entries.
Deselect All :kbd:`Alt-A`
   Deselects all entries.
Invert Selection :kbd:`Ctrl-I`
   Selects non-selected entries and deselects selected ones.
Toggle Selection
   Selects all entries if there are currently no selected ones,
   and deselects them otherwise.
Box Select :kbd:`B`
   Lets you drag a box and adds the entries  that overlap it to the selection.
Delete :kbd:`X`, :kbd:`Delete`
   Removes the selected entries from the log.
Copy :kbd:`Ctrl-C`
   Copies the selected entries to the clipboard.
