.. _bpy.types.NodeGroup:

***********
Node Groups
***********

.. figure:: /images/interface_controls_nodes_groups_example.png
   :align: right

   Example of a node group.

Grouping nodes can simplify a node tree by hiding away complexity and reusing repetitive parts.

Conceptually, node groups allow you to treat a *set* of nodes as though it were just one node.
They're similar to functions in programming:
they can be reused (even in different node trees) and can be customized by changing their "parameters."

As an example, say you created a "Wood" material that you would like to have in different colors.
One way of doing this would be to duplicate the entire material for every color, but if you did that,
you'd have to go over all those copies again if you later wanted to change the density of the grain lines.
Instead, it would be better to move the nodes that define the wood look into a node group.
Each material can then reuse this node group and just supply it with a color.
If you then later want to change the grain line density, you only have to do it once inside the node group,
rather than for every material.

Node groups can be nested (that is, node groups can contain other node groups).

.. note::

   Recursive node groups are prohibited for all the current node systems to prevent infinite recursion.
   A node group can never contain itself (or another group that contains it).

.. tip::

   Like all data-blocks, node groups with names that start with ``.`` are normally hidden from
   :ref:`lists and menus <ui-data-block>` and can only be accessed through search.
   This can be useful for node asset authors to hide their internal sub-groups from the final user.

When a node group is created, new *Group Input* and *Group Output* nodes are generated
to represent the data flow into and out of the group. Furthermore connections to input sockets coming
from unselected nodes will become attached to new sockets on the *Group Input* node.
Similarly, outgoing connections to input sockets of unselected nodes will become attached to
the new *Group Output* node.

If you want to pass an additional parameter into the group,
a socket must be added to the *Group Input* node.
To do this, drag a connection from the hollow socket on the right side of the *Group Input* node
to the desired input socket on the node requiring an input.
The process is similar for the *Group Output* regarding data
you want to be made available outside the group.


Properties
==========

Group
-----

.. reference::

   :Panel:     :menuselection:`Sidebar --> Group --> Group`

.. figure:: /images/interface_controls_nodes_groups_interface-group-panel.png
   :align: right

   The *Group* panel.

This panel contains properties that relate the group node such as it's name and look.

Name
   The name of node as displayed in the :ref:`interface-nodes-parts-title`.

.. _bpy.types.NodeTree.description:

Description
   The message displayed when hovering over the :ref:`interface-nodes-parts-title` or in add menus.

.. _bpy.types.NodeTree.color_tag:

Color Tag
   Color tag of the node group which influences the header color.


Usage :guilabel:`Geometry Nodes`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This panel is only visible in the :doc:`Geometry Node Editor </editors/geometry_node>`.

.. _bpy.types.GeometryNodeTree.is_modifier:

Modifier
   The node group is used as a :doc:`/modeling/modifiers/generate/geometry_nodes`.

.. _bpy.types.GeometryNodeTree.is_tool:

Tool
   The node group is used as a :doc:`/modeling/geometry_nodes/tools`.


.. _bpy.ops.node.tree_socket_add:
.. _bpy.ops.node.tree_socket_remove:
.. _bpy.ops.node.tree_socket_move:

Group Sockets
-------------

.. reference::

   :Panel:     :menuselection:`Sidebar --> Group --> Group Sockets`

.. figure:: /images/interface_controls_nodes_groups_interface-group_sockets_panel.png
   :align: right

   The *Group Sockets* panel.

This panel is used to add, remove, reorder, and edit the sockets of the group's input and output.

.. _bpy.types.NodeTreeInterfaceSocket.name:

Socket List
   A :ref:`ui-list-view` of all inputs, outputs and panels.

   Here you can name the socket which is displayed in the node's interface.

.. _bpy.types.NodeTreeInterfaceSocket.description:

Description
   The message displayed when hovering over socket properties.

.. _bpy.types.NodeTreeInterfacePanel.default_closed:

Closed by Default :guilabel:`Panels`
   Panel is closed by default on new nodes.

.. _bpy.types.NodeTreeInterfaceSocket*.default_value:

Default
   The value to use when nothing is connected to the socket.

.. _bpy.types.NodeTreeInterfaceSocket*.min_value:
.. _bpy.types.NodeTreeInterfaceSocket*.max_value:

Min, Max
   The minimum and maximum value for the UI button shown in the node interface.
   Note, this is not a minimum or maximum for the data that can pass through the node.
   If a socket passes a higher value than the maximum, it will still pass into the node unchanged.

.. rubric:: Geometry Nodes

.. _bpy.types.NodeTreeInterfaceSocket.default_input:

Default Input
   Input to use when the socket is unconnected.
   Requires *Hide Value* to be enabled.

.. _bpy.types.NodeTreeInterfaceSocket.hide_value:

Hide Value
   Hide the socket value even when the socket is not connected.

.. _bpy.types.NodeTreeInterfaceSocket.hide_in_modifier:

Hide in Modifier
   Don't show the input value in the geometry nodes modifier interface.
   This allows the input to be used in the context of a node group but not as a modifier input.

   This option is only available for geometry nodes and only for input sockets.

.. _bpy.types.NodeTreeInterfaceSocket.force_non_field:

Single Value
   Only allow single value inputs rather than :doc:`/modeling/geometry_nodes/fields`.


.. _bpy.ops.node.group_make:

Make Group
==========

.. reference::

   :Menu:      :menuselection:`Node --> Make Group`
   :Shortcut:  :kbd:`Ctrl-G`

To create a node group, select the nodes you want to include, then
press :kbd:`Ctrl-G` or click :menuselection:`Group --> Make Group`.
A node group will have a green title bar. All selected nodes will now be contained within the node group.
Default naming for the node group is "NodeGroup", "NodeGroup.001" etc.
There is a name field in the node group you can click into to change the name of the group.
Change the name of the node group to something meaningful.

When appending node groups from one blend-file to another,
Blender does not make a distinction between material node groups or composite node groups.
So it is recommended to use some naming convention that will allow you to distinguish between the two types.

.. tip::

   The "Add" menu of each node editor contains an "Output" category with node types such as "Material Output."
   These node types should not be confused with the "Group Output" node found in node groups,
   and should not be used in node groups either (only in the top-level node tree).


.. _bpy.ops.node.group_insert:

Insert Into Group
=================

.. reference::

   :Menu:      :menuselection:`Node --> Insert Into Group`

Moves the selected nodes into the :term:`active <Active>` group node.
To use, select a set of nodes, ending with the destination group node,
then, running the operation will move those nodes into that group.
The moved nodes are collected into a group of their own to preserve their connection context,
having their own group input and output nodes.
The group's existing input and output nodes are updated with new sockets, if any, from the new nodes.
The node group must be edited to contain a single *Group Input* and a single *Group Output* node.


.. _bpy.ops.node.tree_path_parent:
.. _bpy.ops.node.group_edit:

Edit Group
==========

.. reference::

   :Menu:      :menuselection:`Node --> Edit Group`
   :Header:    :menuselection:`Go to Parent Node Tree`
   :Shortcut:  :kbd:`Tab`, :kbd:`Ctrl-Tab`

With a node group selected, press :kbd:`Tab` to move into it and see its content.
Press :kbd:`Tab` again (or :kbd:`Ctrl-Tab`) to leave the group and go back to
its parent, which could be the top-level node tree or another node group.
You can refer to the breadcrumbs in the top left corner of the node editor
to see where you are in the hierarchy.

.. figure:: /images/render_cycles_optimizations_reducing-noise_glass-group.png
   :width: 620px

   Example of an expanded node group.


.. _bpy.ops.node.group_ungroup:

Ungroup
=======

.. reference::

   :Menu:      :menuselection:`Node --> Ungroup`
   :Shortcut:  :kbd:`Ctrl-Alt-G`

Removes the group and places the individual nodes into your editor workspace.
No internal connections are lost, and now you can link internal nodes to other nodes in your workspace.

Separate :kbd:`P`
   Separate selected nodes from the node group.

   Copy
      Copy to parent node tree, keep group intact.
   Move
      Move to parent node tree, remove from group.


Reusing Node Groups
===================

.. reference::

   :Menu:      :menuselection:`Add --> Group`
   :Shortcut:  :kbd:`Shift-A`

Existing node groups can be placed again after they're initially defined, be it in the same
node tree or a different one. It's also possible to import node groups from a different blend-file
using :menuselection:`File --> Link/Append`.
