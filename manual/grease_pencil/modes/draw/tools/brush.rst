
*****
Brush
*****

.. reference::

   :Mode:      Draw Mode
   :Tool:      :menuselection:`Toolbar --> Brush`


Tool to use for any of the Grease Pencil *Draw* mode brushes. Activating a brush from an asset
shelf or brush selector will also activate this tool for convenience.

See :doc:`brushes </grease_pencil/modes/draw/brushes/index>` (or the
:doc:`overview </grease_pencil/modes/draw/brushes/overview>`) for a detailed list of all brushes
and their options.

Tool Settings
=============

Default Eraser Brush
   Select a brush to use as eraser for quickly alternating with the main brush using
   :kbd:`Ctrl-LMB`.
