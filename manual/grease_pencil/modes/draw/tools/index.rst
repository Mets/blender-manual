.. _bpy.types.BrushGpencilSettings:

#########
  Tools
#########

.. toctree::
   :maxdepth: 2

   brush.rst
   cutter.rst
   eyedropper.rst
   line.rst
   polyline.rst
   arc.rst
   curve.rst
   box.rst
   circle.rst
   interpolate.rst
