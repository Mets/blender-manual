
#####################
  Vertex Paint Mode
#####################

.. toctree::
   :maxdepth: 2

   introduction.rst
   brushes.rst
   tools.rst
   tool_settings/brush.rst
   editing.rst
