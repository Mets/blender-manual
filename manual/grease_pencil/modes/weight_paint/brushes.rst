
********************
Weight Paint Brushes
********************

Brushes for Grease Pencil *Weight Paint* mode bundled in the *Essentials* library.

Paint Point Weight
   Paints a specified weight over the strokes.

Blur Point Weight
   Smooths out the weighting of adjacent points. In this mode the Weight
   Value is ignored. The strength defines how much the smoothing is applied.

Average Point Weight
   Smooths weights by painting the average resulting weight from all weights under the brush.

Smear Point Weight
   Smudges weights by grabbing the weights under the brush and "dragging" them.
   This can be imagined as a finger painting tool.
